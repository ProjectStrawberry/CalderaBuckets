package com.calderaminecraft.calderabuckets;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalderaBuckets extends JavaPlugin {
    CalderaBuckets plugin;

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        FileConfiguration config = this.getConfig();
        Map<Integer, String> recipes = new HashMap<>();
        config.options().copyDefaults(true);
        saveConfig();

        for (String data : config.getStringList("recipes")) {
            String[] splitData = data.split(":");
            if (splitData.length < 2) {
                getLogger().warning("Invalid recipe configuration: " + data);
                continue;
            }
            int size ;
            try {
                size = Integer.parseInt(splitData[0]);
            } catch (NumberFormatException e) {
                getLogger().warning("Invalid size in recipe configuration: " + data);
                continue;
            }
            recipes.put(size, splitData[1]);
        }

        for (Map.Entry<Integer, String> entry : recipes.entrySet()) {
            Integer size = entry.getKey();
            String value = entry.getValue();

            Material recipeMaterial = Material.getMaterial(value);
            if (recipeMaterial == null) {
                getLogger().warning("Invalid recipe material in configuration: " + value);
                continue;
            }

            ItemStack bucket = new ItemStack(Material.BUCKET, 1);
            ItemMeta bucketMeta = bucket.getItemMeta();
            bucketMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
            bucketMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

            List<String> lore = new ArrayList<>();
            lore.add("");
            lore.add(ChatColor.DARK_AQUA + "Filled: 0");
            lore.add(ChatColor.DARK_AQUA + "Capacity: " + size);
            lore.add(ChatColor.DARK_AQUA + "Contains: Empty");
            bucketMeta.setLore(lore);
            bucket.setItemMeta(bucketMeta);

            ShapedRecipe recipe = new ShapedRecipe(new NamespacedKey(plugin, size.toString()), bucket);
            recipe.shape("###", "IXI", "#I#");
            recipe.setIngredient('I', Material.IRON_INGOT);
            recipe.setIngredient('X', recipeMaterial);

            getServer().addRecipe(recipe);
            getLogger().info("Added recipe for a bucket with the size of " + size);
        }

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        this.getCommand("calderabuckets").setExecutor(new CommandBuckets(this));

        getLogger().info("CalderaBuckets loaded.");
    }

    @Override
    public void onDisable() {

    }

    int getMaxSize(Player player, String permission) {
        int size = plugin.getConfig().getInt("maxsize");
        int maxSize = 0;
        for (int i = 0; i <= size; i++) {
            if (player.hasPermission("caldera.buckets." + permission + "." + i)) {
                maxSize = i;
            }
        }
        return maxSize;
    }
}
