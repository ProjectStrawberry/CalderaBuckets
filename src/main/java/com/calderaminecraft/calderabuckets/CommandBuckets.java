package com.calderaminecraft.calderabuckets;

import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagInt;
import net.minecraft.server.v1_13_R2.NBTTagString;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class CommandBuckets implements CommandExecutor {
    private CalderaBuckets plugin;

    CommandBuckets(CalderaBuckets p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }
        if (!sender.hasPermission("caldera.buckets.spawn")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }
        if (args.length < 1) return false;
        Player player = (Player) sender;

        int size;
        int maxSize = plugin.getConfig().getInt("maxsize");

        try {
            size = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid size.");
            return true;
        }

        if (size > maxSize) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The maximum size a bucket can be is " + ChatColor.RED + maxSize);
            return true;
        }

        ItemStack bucket = new ItemStack(Material.BUCKET, 1);
        ItemMeta bucketMeta = bucket.getItemMeta();
        bucketMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
        bucketMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

        List<String> lore = new ArrayList<>();
        lore.add("");
        lore.add(ChatColor.DARK_AQUA + "Filled: 0");
        lore.add(ChatColor.DARK_AQUA + "Capacity: " + size);
        lore.add(ChatColor.DARK_AQUA + "Contains: Empty");
        bucketMeta.setLore(lore);
        bucket.setItemMeta(bucketMeta);

        net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(bucket);
        NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

        itemCompound.set("cbItem", new NBTTagString("true"));
        itemCompound.set("cbFilled", new NBTTagInt(0));
        itemCompound.set("cbCapacity", new NBTTagInt(size));
        itemCompound.set("cbContains", new NBTTagString("Empty"));

        nmsItem.setTag(itemCompound);
        bucket = CraftItemStack.asBukkitCopy(nmsItem);

        Inventory inv = player.getInventory();
        if (inv.firstEmpty() == -1) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Your inventory is full.");
            return true;
        }
        inv.addItem(bucket);
        sender.sendMessage(ChatColor.GOLD + "You've received a " + ChatColor.RED + "Caldera Bucket" + ChatColor.GOLD + " with the size of " + ChatColor.RED + size);

        return true;
    }
}