package com.calderaminecraft.calderabuckets;

import net.minecraft.server.v1_13_R2.NBTTagCompound;
import net.minecraft.server.v1_13_R2.NBTTagInt;
import net.minecraft.server.v1_13_R2.NBTTagString;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.CauldronLevelChangeEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.FurnaceBurnEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.*;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventListeners implements Listener {
    private CalderaBuckets plugin;
    private Map<String, ItemStack> buckets = new HashMap<>();

    EventListeners(CalderaBuckets p) {
        plugin = p;
    }

   @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
       if (event == null || event.getHand() == null || !event.getHand().equals(EquipmentSlot.HAND)) return;
       ItemStack item = event.getItem();
       if (item == null || !item.hasItemMeta()) return;
       Material bucket = item.getType();
       Player player = event.getPlayer();
       Action action = event.getAction();
       Block block = event.getClickedBlock();
       boolean clickedWater = false;
       boolean clickedLava = false;

       if (action == Action.RIGHT_CLICK_BLOCK || action == Action.RIGHT_CLICK_AIR) {
           List<Block> lineOfSight = event.getPlayer().getLineOfSight(null, 5);
           for (Block b : lineOfSight) {
               if (b.getType() == Material.WATER) {
                   clickedWater = true;
                   break;
               }
               if (b.getType() == Material.LAVA) {
                   clickedLava = true;
                   break;
               }
           }
       }

       net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
       NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

       if (itemCompound == null || !itemCompound.getString("cbItem").equals("true")) return;

       int capacity = itemCompound.getInt("cbCapacity");
       int filled = itemCompound.getInt("cbFilled");
       String filledWith = itemCompound.getString("cbContains");

       if (capacity > plugin.getMaxSize(player, "use")) {
           player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have permission to use a bucket this size.");
           event.setCancelled(true);
           return;
       }

       if ((action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK) && (filled > 0 && filled != capacity) && player.isSneaking()) {
           if (bucket.equals(Material.BUCKET)) {
               List<String> newLore = new ArrayList<>();

               Material materialType = Material.BUCKET;
               if (filledWith.equals("Water")) materialType = Material.WATER_BUCKET;
               if (filledWith.equals("Lava")) materialType = Material.LAVA_BUCKET;
               if (filledWith.equals("Milk")) materialType = Material.MILK_BUCKET;

               ItemStack newItem = new ItemStack(materialType, 1);
               ItemMeta newMeta = newItem.getItemMeta();
               newMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
               newMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

               newLore.add("");
               newLore.add(ChatColor.DARK_AQUA + "Filled: " + filled);
               newLore.add(ChatColor.DARK_AQUA + "Capacity: " + capacity);
               newLore.add(ChatColor.DARK_AQUA + "Contains: " + filledWith);

               newMeta.setLore(newLore);
               newItem.setItemMeta(newMeta);

               net.minecraft.server.v1_13_R2.ItemStack newNmsItem = CraftItemStack.asNMSCopy(newItem);
               NBTTagCompound newItemCompound = (newNmsItem.hasTag() ? newNmsItem.getTag() : new NBTTagCompound());

               newItemCompound.set("cbItem", new NBTTagString("true"));
               newItemCompound.set("cbFilled", new NBTTagInt(filled));
               newItemCompound.set("cbCapacity", new NBTTagInt(capacity));
               newItemCompound.set("cbContains", new NBTTagString(filledWith));

               newNmsItem.setTag(newItemCompound);
               newItem = CraftItemStack.asBukkitCopy(newNmsItem);

               player.getInventory().setItemInMainHand(newItem);
               event.setCancelled(true);
               return;
           }

           if (bucket.equals(Material.LAVA_BUCKET) || bucket.equals(Material.WATER_BUCKET) || bucket.equals(Material.MILK_BUCKET)) {
               List<String> newLore = new ArrayList<>();

               ItemStack newItem = new ItemStack(Material.BUCKET, 1);
               ItemMeta newMeta = newItem.getItemMeta();
               newMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
               newMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

               newLore.add("");
               newLore.add(ChatColor.DARK_AQUA + "Filled: " + filled);
               newLore.add(ChatColor.DARK_AQUA + "Capacity: " + capacity);
               newLore.add(ChatColor.DARK_AQUA + "Contains: " + filledWith);

               newMeta.setLore(newLore);
               newItem.setItemMeta(newMeta);

               net.minecraft.server.v1_13_R2.ItemStack newNmsItem = CraftItemStack.asNMSCopy(newItem);
               NBTTagCompound newItemCompound = (newNmsItem.hasTag() ? newNmsItem.getTag() : new NBTTagCompound());

               newItemCompound.set("cbItem", new NBTTagString("true"));
               newItemCompound.set("cbFilled", new NBTTagInt(filled));
               newItemCompound.set("cbCapacity", new NBTTagInt(capacity));
               newItemCompound.set("cbContains", new NBTTagString(filledWith));

               newNmsItem.setTag(newItemCompound);
               newItem = CraftItemStack.asBukkitCopy(newNmsItem);

               player.getInventory().setItemInMainHand(newItem);
               event.setCancelled(true);
               return;
           }
       }

       if (bucket.equals(Material.BUCKET) && (clickedWater || clickedLava || (block != null && block.getType().equals(Material.CAULDRON)))) {
           if ((filledWith.equals("Water") && !(clickedWater || (block != null && block.getType().equals(Material.CAULDRON))) || (filledWith.equals("Lava") && !clickedLava)) || filledWith.equals("Milk")) {
               player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Your bucket contains " + ChatColor.RED + filledWith);
               event.setCancelled(true);
               return;
           }

           if (filled >= capacity) {
               player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This bucket is full!");
               event.setCancelled(true);
               return;
           }

           if ((clickedWater || (block != null && block.getType().equals(Material.CAULDRON))) && !player.hasPermission("caldera.buckets.liquids.water")) {
               player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have permission to use this bucket with water.");
               event.setCancelled(true);
               return;
           }

           if (clickedLava && !player.hasPermission("caldera.buckets.liquids.lava")) {
               player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have permission to use this bucket with lava.");
               event.setCancelled(true);
               return;
           }

           if (filledWith.equals("Empty")) {
               if (clickedWater) filledWith = "Water";
               if (clickedLava) filledWith = "Lava";
               if ((block != null && block.getType().equals(Material.CAULDRON))) filledWith = "Water";
           }

           filled++;

           List<String> newLore = new ArrayList<>();

           Material materialType = Material.BUCKET;
           if (filled == capacity && filledWith.equals("Water")) materialType = Material.WATER_BUCKET;
           if (filled == capacity && filledWith.equals("Lava")) materialType = Material.LAVA_BUCKET;

           ItemStack newItem = new ItemStack(materialType, 1);
           ItemMeta newMeta = newItem.getItemMeta();
           newMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
           newMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

           newLore.add("");
           newLore.add(ChatColor.DARK_AQUA + "Filled: " + filled);
           newLore.add(ChatColor.DARK_AQUA + "Capacity: " + capacity);
           newLore.add(ChatColor.DARK_AQUA + "Contains: " + filledWith);

           newMeta.setLore(newLore);
           newItem.setItemMeta(newMeta);

           net.minecraft.server.v1_13_R2.ItemStack newNmsItem = CraftItemStack.asNMSCopy(newItem);
           NBTTagCompound newItemCompound = (newNmsItem.hasTag() ? newNmsItem.getTag() : new NBTTagCompound());

           newItemCompound.set("cbItem", new NBTTagString("true"));
           newItemCompound.set("cbFilled", new NBTTagInt(filled));
           newItemCompound.set("cbCapacity", new NBTTagInt(capacity));
           newItemCompound.set("cbContains", new NBTTagString(filledWith));

           newNmsItem.setTag(newItemCompound);
           newItem = CraftItemStack.asBukkitCopy(newNmsItem);

           buckets.put(player.getUniqueId().toString(), newItem);
       }

       if (bucket.equals(Material.WATER_BUCKET) || bucket.equals(Material.LAVA_BUCKET)) {
           if (clickedWater && !player.hasPermission("caldera.buckets.liquids.water")) {
               player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have permission to use this bucket with water.");
               event.setCancelled(true);
               return;
           }

           if (clickedWater && !player.hasPermission("caldera.buckets.liquids.lava")) {
               player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have permission to use this bucket with lava.");
               event.setCancelled(true);
               return;
           }
            filled--;

           List<String> newLore = new ArrayList<>();

           Material materialType = Material.BUCKET;
           if (filled > 0 && filledWith.equals("Water")) materialType = Material.WATER_BUCKET;
           if (filled > 0 && filledWith.equals("Lava")) materialType = Material.LAVA_BUCKET;
           if (filled == 0) filledWith = "Empty";

           ItemStack newItem = new ItemStack(materialType, 1);
           ItemMeta newMeta = newItem.getItemMeta();
           newMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
           newMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

           newLore.add("");
           newLore.add(ChatColor.DARK_AQUA + "Filled: " + filled);
           newLore.add(ChatColor.DARK_AQUA + "Capacity: " + capacity);
           newLore.add(ChatColor.DARK_AQUA + "Contains: " + filledWith);

           newMeta.setLore(newLore);
           newItem.setItemMeta(newMeta);

           net.minecraft.server.v1_13_R2.ItemStack newNmsItem = CraftItemStack.asNMSCopy(newItem);
           NBTTagCompound newItemCompound = (newNmsItem.hasTag() ? newNmsItem.getTag() : new NBTTagCompound());

           newItemCompound.set("cbItem", new NBTTagString("true"));
           newItemCompound.set("cbFilled", new NBTTagInt(filled));
           newItemCompound.set("cbCapacity", new NBTTagInt(capacity));
           newItemCompound.set("cbContains", new NBTTagString(filledWith));

           newNmsItem.setTag(newItemCompound);
           newItem = CraftItemStack.asBukkitCopy(newNmsItem);

           buckets.put(player.getUniqueId().toString(), newItem);
       }
   }
   @EventHandler
   public void PlayerInteractEntityEvent(PlayerInteractEntityEvent event) {
       if (!event.getHand().equals(EquipmentSlot.HAND)) return;
       Player player = event.getPlayer();
       ItemStack item = player.getInventory().getItemInMainHand();
       if (item == null || !(event.getRightClicked() instanceof Cow)) return;
       Material bucket = item.getType();

       net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
       NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

       if (itemCompound == null || !itemCompound.getString("cbItem").equals("true")) return;

       int capacity = itemCompound.getInt("cbCapacity");
       int filled = itemCompound.getInt("cbFilled");
       String filledWith = itemCompound.getString("cbContains");

       if (capacity > plugin.getMaxSize(player, "use")) {
           player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have permission to use a bucket this size.");
           event.setCancelled(true);
           player.updateInventory();
           return;
       }

       if (bucket.equals(Material.BUCKET)) {
           if (filledWith.equals("Water") || filledWith.equals("Lava")) {
               player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Your bucket contains " + ChatColor.RED + filledWith);
               event.setCancelled(true);
               player.updateInventory();
               return;
           }

           if (filled >= capacity) {
               player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This bucket is full!");
               event.setCancelled(true);
               player.updateInventory();
               return;
           }

           if (!player.hasPermission("caldera.buckets.liquids.milk")) {
               player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have permission to use this bucket with milk.");
               event.setCancelled(true);
               player.updateInventory();
               return;
           }

           if (filledWith.equals("Empty")) filledWith = "Milk";

           filled++;

           List<String> newLore = new ArrayList<>();

           Material materialType = Material.BUCKET;
           if (filled == capacity) materialType = Material.MILK_BUCKET;

           ItemStack newItem = new ItemStack(materialType, 1);
           ItemMeta newMeta = newItem.getItemMeta();
           newMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
           newMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

           newLore.add("");
           newLore.add(ChatColor.DARK_AQUA + "Filled: " + filled);
           newLore.add(ChatColor.DARK_AQUA + "Capacity: " + capacity);
           newLore.add(ChatColor.DARK_AQUA + "Contains: " + filledWith);

           newMeta.setLore(newLore);
           newItem.setItemMeta(newMeta);

           net.minecraft.server.v1_13_R2.ItemStack newNmsItem = CraftItemStack.asNMSCopy(newItem);
           NBTTagCompound newItemCompound = (newNmsItem.hasTag() ? newNmsItem.getTag() : new NBTTagCompound());

           newItemCompound.set("cbItem", new NBTTagString("true"));
           newItemCompound.set("cbFilled", new NBTTagInt(filled));
           newItemCompound.set("cbCapacity", new NBTTagInt(capacity));
           newItemCompound.set("cbContains", new NBTTagString(filledWith));

           newNmsItem.setTag(newItemCompound);
           newItem = CraftItemStack.asBukkitCopy(newNmsItem);

           buckets.put(player.getUniqueId().toString(), newItem);
       }

       if (bucket.equals(Material.WATER_BUCKET)) {
           filled--;

           List<String> newLore = new ArrayList<>();

           Material materialType = Material.BUCKET;
           if (filled > 0 && filledWith.equals("Water")) materialType = Material.WATER_BUCKET;
           if (filled > 0 && filledWith.equals("Lava")) materialType = Material.LAVA_BUCKET;
           if (filled == 0) filledWith = "Empty";

           ItemStack newItem = new ItemStack(materialType, 1);
           ItemMeta newMeta = newItem.getItemMeta();
           newMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
           newMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

           newLore.add("");
           newLore.add(ChatColor.DARK_AQUA + "Filled: " + filled);
           newLore.add(ChatColor.DARK_AQUA + "Capacity: " + capacity);
           newLore.add(ChatColor.DARK_AQUA + "Contains: " + filledWith);

           newMeta.setLore(newLore);
           newItem.setItemMeta(newMeta);

           net.minecraft.server.v1_13_R2.ItemStack newNmsItem = CraftItemStack.asNMSCopy(newItem);
           NBTTagCompound newItemCompound = (newNmsItem.hasTag() ? newNmsItem.getTag() : new NBTTagCompound());

           newItemCompound.set("cbItem", new NBTTagString("true"));
           newItemCompound.set("cbFilled", new NBTTagInt(filled));
           newItemCompound.set("cbCapacity", new NBTTagInt(capacity));
           newItemCompound.set("cbContains", new NBTTagString(filledWith));

           newNmsItem.setTag(newItemCompound);
           newItem = CraftItemStack.asBukkitCopy(newNmsItem);

           buckets.put(player.getUniqueId().toString(), newItem);
       }
   }
   @EventHandler
   public void PlayerItemConsumeEvent(PlayerItemConsumeEvent event) {
       final Player player = event.getPlayer();
       ItemStack item = event.getItem();

       net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
       NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

       if (itemCompound == null || !itemCompound.getString("cbItem").equals("true")) return;

       int capacity = itemCompound.getInt("cbCapacity");
       int filled = itemCompound.getInt("cbFilled");
       String filledWith = itemCompound.getString("cbContains");

       if (capacity > plugin.getMaxSize(player, "use")) {
           player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have permission to use a bucket this size.");
           event.setCancelled(true);
           return;
       }

       if (event.getItem().getType().equals(Material.MILK_BUCKET)) {
           player.getInventory().all(event.getItem());

           filled--;

           List<String> newLore = new ArrayList<>();

           Material materialType = Material.BUCKET;
           if (filled > 0 && filledWith.equals("Water")) materialType = Material.WATER_BUCKET;
           if (filled > 0 && filledWith.equals("Lava")) materialType = Material.LAVA_BUCKET;
           if (filled > 0 && filledWith.equals("Milk")) materialType = Material.MILK_BUCKET;
           if (filled == 0) filledWith = "Empty";

           ItemStack newItem = new ItemStack(materialType, 1);
           ItemMeta newMeta = newItem.getItemMeta();
           newMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
           newMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

           newLore.add("");
           newLore.add(ChatColor.DARK_AQUA + "Filled: " + filled);
           newLore.add(ChatColor.DARK_AQUA + "Capacity: " + capacity);
           newLore.add(ChatColor.DARK_AQUA + "Contains: " + filledWith);

           newMeta.setLore(newLore);
           newItem.setItemMeta(newMeta);

           net.minecraft.server.v1_13_R2.ItemStack newNmsItem = CraftItemStack.asNMSCopy(newItem);
           NBTTagCompound newItemCompound = (newNmsItem.hasTag() ? newNmsItem.getTag() : new NBTTagCompound());

           newItemCompound.set("cbItem", new NBTTagString("true"));
           newItemCompound.set("cbFilled", new NBTTagInt(filled));
           newItemCompound.set("cbCapacity", new NBTTagInt(capacity));
           newItemCompound.set("cbContains", new NBTTagString(filledWith));

           newNmsItem.setTag(newItemCompound);
           newItem = CraftItemStack.asBukkitCopy(newNmsItem);

           final ItemStack newerItem = newItem;

           new BukkitRunnable() {
               @Override
               public void run() {
                   player.getInventory().setItemInMainHand(newerItem);
                   player.updateInventory();

               }
           }.runTaskLater(plugin, 1);
           buckets.remove(player.getUniqueId().toString());
       }
   }
   @EventHandler
   public void FurnaceBurnEvent(FurnaceBurnEvent event) {
       ItemStack item = event.getFuel();
       Block block = event.getBlock();
       InventoryHolder ih = (InventoryHolder) block.getState();
       Inventory inventory = ih.getInventory();
       final FurnaceInventory inv = (FurnaceInventory) inventory;

       net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
       NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

       if (itemCompound == null || !itemCompound.getString("cbItem").equals("true")) return;

       int capacity = itemCompound.getInt("cbCapacity");
       int filled = itemCompound.getInt("cbFilled");
       String filledWith = itemCompound.getString("cbContains");

       if (item.getType().equals(Material.LAVA_BUCKET)) {
           filled--;

           List<String> newLore = new ArrayList<>();

           Material materialType = Material.BUCKET;
           if (filled > 0 && filledWith.equals("Lava")) materialType = Material.LAVA_BUCKET;
           if (filled == 0) filledWith = "Empty";

           ItemStack newItem = new ItemStack(materialType, 1);
           ItemMeta newMeta = newItem.getItemMeta();
           newMeta.setDisplayName(ChatColor.AQUA + "Caldera Bucket");
           newMeta.addEnchant(Enchantment.WATER_WORKER, 3, true);

           newLore.add("");
           newLore.add(ChatColor.DARK_AQUA + "Filled: " + filled);
           newLore.add(ChatColor.DARK_AQUA + "Capacity: " + capacity);
           newLore.add(ChatColor.DARK_AQUA + "Contains: " + filledWith);

           newMeta.setLore(newLore);
           newItem.setItemMeta(newMeta);

           net.minecraft.server.v1_13_R2.ItemStack newNmsItem = CraftItemStack.asNMSCopy(newItem);
           NBTTagCompound newItemCompound = (newNmsItem.hasTag() ? newNmsItem.getTag() : new NBTTagCompound());

           newItemCompound.set("cbItem", new NBTTagString("true"));
           newItemCompound.set("cbFilled", new NBTTagInt(filled));
           newItemCompound.set("cbCapacity", new NBTTagInt(capacity));
           newItemCompound.set("cbContains", new NBTTagString(filledWith));

           newNmsItem.setTag(newItemCompound);
           newItem = CraftItemStack.asBukkitCopy(newNmsItem);

           final ItemStack newerItem = newItem;

           new BukkitRunnable() {
               @Override
               public void run() {
                   inv.setFuel(newerItem);
               }
           }.runTaskLater(plugin, 1);
       }
   }
   @EventHandler
   public void CraftItemEvent(CraftItemEvent event) {
       ItemStack item = event.getRecipe().getResult();
       if (item == null || !item.hasItemMeta()) return;
       final Player player = (Player) event.getWhoClicked();
       net.minecraft.server.v1_13_R2.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
       NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

       if (itemCompound == null || !itemCompound.getString("cbItem").equals("true")) return;

       int capacity = itemCompound.getInt("cbCapacity");

       if (capacity > plugin.getMaxSize(player, "craft")) {
           player.getOpenInventory().close();
           player.updateInventory();
           player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't have permission to craft a bucket this size.");
           event.setCancelled(true);
       }
   }
   @EventHandler
   public void CauldronLevelChangeEvent(CauldronLevelChangeEvent event) {
       if (!(event.getEntity() instanceof Player)) return;
       final Player player = (Player) event.getEntity();
       if (buckets.containsKey(player.getUniqueId().toString())) {
           final ItemStack bucket = buckets.get(player.getUniqueId().toString());
           new BukkitRunnable() {
               @Override
               public void run() {
                   player.getInventory().setItemInMainHand(bucket);
                   player.updateInventory();

               }
           }.runTaskLater(plugin, 1);
           buckets.remove(player.getUniqueId().toString());
       }
   }
   @EventHandler
    public void PlayerBucketFillEvent(PlayerBucketFillEvent event) {
       Player player = event.getPlayer();
       if (buckets.containsKey(player.getUniqueId().toString())) {
           event.setItemStack(buckets.get(player.getUniqueId().toString()));
           buckets.remove(player.getUniqueId().toString());
       }
   }
   @EventHandler
    public void PlayerBucketEmptyEvent(PlayerBucketEmptyEvent event) {
        final Player player = event.getPlayer();
        if (buckets.containsKey(player.getUniqueId().toString())) {
            final ItemStack bucket = buckets.get(player.getUniqueId().toString());
            new BukkitRunnable() {
                @Override
                public void run() {
                    player.getInventory().setItemInMainHand(bucket);
                    player.updateInventory();
                }
            }.runTaskLater(plugin, 1);
            buckets.remove(player.getUniqueId().toString());
        }
    }
}